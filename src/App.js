import React, { Component } from 'react';
import * as _ from 'lodash'
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

var possibleCombinationSum = function(arr, n) {
  if (arr.indexOf(n) >= 0) { return true; }
  if (arr[0] > n) { return false; }
  if (arr[arr.length - 1] > n) {
    arr.pop();
    return possibleCombinationSum(arr, n);
  }
  var listSize = arr.length, combinationsCount = (1 << listSize)
  for (var i = 1; i < combinationsCount ; i++ ) {
    var combinationSum = 0;
    for (var j=0 ; j < listSize ; j++) {
      if (i & (1 << j)) { combinationSum += arr[j]; }
    }
    if (n === combinationSum) { return true; }
  }
  return false;
}


const Stars = (props) => {
  return (
    <div className="col-5">
      {_.range(props.numberOfStars).map((i) => 
        <i key={i} className="fa fa-star"></i>
      )}
    </div>
  )
}

const Button = (props) => {
  let button;

  switch(props.anwserIsCorrect) {
    case true: 
      button = 
        <button className="btn btn-success" onClick={props.acceptAnswer}>
          <i className="fa fa-check"></i>
        </button>
      break;      
    case false:
      button = 
        <button className="btn btn-danger">
          <i className="fa fa-times"></i>
        </button>
      break;
    default:
      button = <button className="btn" 
                       disabled={props.selectedNumbers.length === 0}
                       onClick={props.checkAnswer}>
                =
               </button>
      break;
  }

  return (
    <div className="col-2">
      {button}
      <br /> <br />
      <button className="btn btn-warning btn-sm"
              onClick={props.redraw}
              disabled={props.redraws === 0}>
        <i className="fa fa-refresh"></i> {props.redraws}
      </button>
    </div>
  )
}

const Anwser = (props) => {
  return (
    <div className="col-5">
      {props.selectedNumbers.map((number, i) => {
        return <span key={i} onClick={() => props.unselectNumber(number)}>
          {number}
        </span>
      })}
    </div>
  )
}

const Number = (props) => {
  const numberClassName = (number) => {
    if (props.usedNumbers.indexOf(number) >= 0) {
      return "used"
    } 
    if (props.selectedNumbers.indexOf(number) >= 0) {
      return "selected"
    } 
  }

  return (
    <div className="card text-center">
      <div>
        {
          Number.list.map((number, index) => {
            return <span key={index} onClick={() => props.selectNumber(number)} className={numberClassName(number)}>{number}</span>
          })
        }
      </div>
    </div> 
  );
};
Number.list = _.range(1, 10);

const DoneFrame = (props) => {
  return (
    <div className="text-center"> 
      <h2>{props.doneStatus}</h2>
      <button className="btn btn-secondary" onClick={props.resetGame}>Play Again</button>
    </div>
  )
}

class Game extends Component {
  static randomNumber = () => 1 + Math.floor(Math.random()*9);
  static initialState = () => ({
    selectedNumbers: [],
    usedNumbers: [],
    randomNumberOfStars: Game.randomNumber(),
    anwserIsCorrect: null,
    redraw: 5,
    doneStatus: null
  });
  
  state = Game.initialState();
  resetGame = () => this.setState(Game.initialState());

  selectNumber = (clickedNumber) => {
    if(this.state.selectedNumbers.indexOf(clickedNumber) >= 0) { return; }

    this.setState(prevState => 
      ({      
        anwserIsCorrect: null,
        selectedNumbers: [ ...prevState.selectedNumbers, clickedNumber ]
      }));
  }

  unselectNumber = (clickedNumber) => {
    this.setState(prevState => ({
      anwserIsCorrect: null,
      selectedNumbers: prevState.selectedNumbers.filter(number => number !== clickedNumber)
    }));
  }

  checkAnswer = () => {
    this.setState(prevState => ({
      anwserIsCorrect: prevState.randomNumberOfStars ===
        prevState.selectedNumbers.reduce((a, b) => a + b, 0)
    }));
  }

  acceptAnswer = () => {
    this.setState(prevState => ({
      usedNumbers: prevState.usedNumbers.concat(prevState.selectedNumbers),
      selectedNumbers: [],
      anwserIsCorrect: null,
      randomNumberOfStars: Game.randomNumber()
    }), this.updateDoneStatus);
  }

  redraw = () => {
    if (this.state.redraw === 0) { return; }
    this.setState(prevState => ({
      randomNumberOfStars: Game.randomNumber(),
      anwserIsCorrect: null,
      selectedNumbers: [],
      redraw: prevState.redraw - 1 
    }), this.updateDoneStatus);    
  }

  possibleSolutions = ({usedNumbers, randomNumberOfStars}) => {
    const possibleNumbers = _.range(1,10).filter(number => 
      usedNumbers.indexOf(number) === -1
    );

    return this.possibleCombinationSum(possibleNumbers, randomNumberOfStars);
  }

  updateDoneStatus = () => {    
    this.setState(prevState => {      
      if (prevState.usedNumbers.length === 9) {        
        return { doneStatus: 'Done. Nice!' };
      } else if (prevState.redraw === 0 && this.possibleSolutions(prevState)) {
        return { doneStatus: 'Game Over!' };
      }
    });
  }

  render() {
    const { selectedNumbers, 
      randomNumberOfStars, 
      anwserIsCorrect, 
      usedNumbers,
      redraw,
      doneStatus } = this.state;
    return (
      <div className="container">
        <h3>Play Nine</h3>
        <hr />
        <div className="row">
          <Stars numberOfStars={randomNumberOfStars}/>
          <Button selectedNumbers={selectedNumbers} 
                  redraws={redraw}
                  checkAnswer={this.checkAnswer}
                  anwserIsCorrect={anwserIsCorrect}
                  acceptAnswer={this.acceptAnswer}
                  redraw={this.redraw}/>
          <Anwser selectedNumbers={selectedNumbers} unselectNumber={this.unselectNumber} />
        </div>
        <br />

        {doneStatus ? 
          <DoneFrame resetGame={this.resetGame} doneStatus={doneStatus}/> :
          <Number selectNumber={this.selectNumber} 
          selectedNumbers={selectedNumbers}
          usedNumbers={usedNumbers} />
        }    
      </div>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div className="app">
        <Game />        
      </div>
    );
  }
}

export default App;
